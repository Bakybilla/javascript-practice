var firstName = 'Jhon';
var lastName = 'Smith';
var age = 30;
console.log(firstName +' '+ lastName +' is '+ age +' years old');

//We can't use a number as the first character in a variable name
// var 1 = 10;
// console.log(1);

//We can't use any symbols instead of $ sign or _ in a variable name
// var -hello = 'hello world';
// console.log(-hello);
// var hello-world = 'hello world';
// console.log(hello-world);
var $num = 10;
console.log($num);
var _num = 30;
console.log(_num);

//we can't use reserved keywords in a variable name
// var function = 20;
// console.log(function);
// var new = 40;
// console.log(new);